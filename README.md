# Parasoft SOAtest Integration for GitLab

This project provides example pipelines that demonstrate how to integrate Parasoft SOAtest with GitLab. The integration enables you to collect functional tests results with Parasoft SOAtest and review results directly in GitLab.

Parasoft SOAtest is a testing tool that automates thorough testing for composite applications with robust support for REST and web services, including over 120 protocols/message types. It is an enterprise-grade solution that simplifies complex testing for business-critical transactions through APIs, message brokers, databases, ERPs, browser-based UIs, and other endpoints.

- Request [a demo](http://www.parasoft.com/products/parasoft-soatest/soatest-request-a-demo/) to see an overview of Parasoft SOAtest's features and benefits.
- See the [user guide](http://docs.parasoft.com/) for information about Parasoft SOAtest's capabilities and usage.

Please visit the [official Parasoft website](http://www.parasoft.com) for more information about Parasoft SOAtest and other Parasoft products.

- [Quick start](#quick-start-FUNC)
- [Example Pipelines](#example-pipelines-FUNC)
- [Reviewing Test Results](#reviewing-test-results)

## <a name="quick-start-FUNC"></a> Quick start

To collect Parasoft SOAtest functional tests results and review test results in GitLab, you need to customize your pipeline to include a job that will:
* run SOAtest.
* use Saxon to convert SOAtest functional tests report to xUnit format.
* upload the transformed xUnit report.

### Prerequisites

* This extension requires Parasoft SOAtest 2020.1 (or newer) with a valid Parasoft license.
* We recommend that you execute the pipeline on a GitLab runner with Parasoft SOAtest installed and configured on the runner.
* To support xUnit format, you need the following files:
  - Saxon-HE: copy from [here](saxon) or download from [Saxonica](http://www.saxonica.com/download/java.xml).
  - [XSLT file](xsl/xunit.xsl) for transforming from Parasoft functional tests report to xUnit report.

## <a name="example-pipelines-FUNC"></a> Example Pipelines

The following example shows a simple pipeline. The example assumes that SOAtest is run on a GitLab runner and the path to the `soatestcli` executable is available on `PATH`.

See also the example [.gitlab-ci.yml](http://gitlab.com/parasoft/soatest-gitlab/-/blob/master/pipelines/.gitlab-ci.yml) file.

```yaml
# This is a basic pipeline to help you get started with SOAtest integration to analyze a GitLab project.

stages:
  - test-soatest
# Runs functional tests with SOAtest.
SOAtest:
  stage: test-soatest
  script:
    # Launches SOAtest.
    - echo "Running SOAtest..."
    - soatestcli -config "builtin://Demo Configuration" -data path/to/workspace -report reports -resource path/to/test/suites

    # Convert SOAtest functional tests report to xUnit format.
    # When running on Windows, be sure to replace backslashes:
    # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/sottest/plugins/com.parasoft.ptest.jdk.eclipse.core.web.win32.x86_64_<VERSION_ID>/jdk/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/xunit.xsl" -s:"reports/report.xml" -o:"reports/report-xunit.xml" -t pipelineBuildWorkingDirectory=${CI_PROJECT_DIR}
    # Notes: To use Saxon for transformation, a Java executable is required. SOAtest is bundled with Java which can be used for this purpose.

  # Uploads SOAtest report in xUnit format
  artifacts:
    reports:
      junit: reports/report-xunit.xml
```

## Reviewing Test Results
When the pipeline completes, you can review the SOAtest test results in the *Tests* tab of the GitLab pipeline.